# Webform Submissions Download

The Webform Submissions Download module is used to download all fields with 
attachment files into a single zip at once.


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Webform](https://www.drupal.org/project/webform)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Go to Structure >> Webform.
2. Create a new webform.
3. Add multiple fields like text, textarea, files, document file, image etc.
4. Submit the form using the test functionality & go to results tab.
5. In the result row, under operations, you will see "Download All Files".
6. Select the "Download All Files" option & you will get the zip file.


## Maintainers

- Ravikant Mane - [ravimane23](https://www.drupal.org/u/ravimane23)
