<?php

namespace Drupal\webform_all_download\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\Entity\Webform;
use Symfony\Component\HttpFoundation\Response;
use Drupal\webform_all_download\Plugin\Archiver\Zip;
use Symfony\Component\Finder\Iterator\RecursiveDirectoryIterator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Provides route responses for Webform testing.
 */
class WebformDownloadAllController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  protected $entityTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('file_system'),
      $container->get('entity_type.manager'),
      $container->get('messenger')
    );
  }

  /**
   * Constructs a webform controller object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(FileSystemInterface $file_system, EntityTypeManagerInterface $entityTypeManager, MessengerInterface $messenger) {
    $this->fileSystem = $file_system;
    $this->entityTypeManager = $entityTypeManager;
    $this->messenger = $messenger;
  }

  /**
   * Returns a webform to add a new test submission to a webform.
   *
   * @param \Drupal\webform\WebformInterface $webform
   *   The webform entity.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission entity.
   *
   * @return object
   *   The response object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Archiver\ArchiverException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function download_download(WebformInterface $webform, WebformSubmissionInterface $webform_submission) {
    $webform_id = $webform->id();
    $webform_submission_id = $webform_submission->id();
    $filename = $webform . '_' . $webform_submission_id . '_files';
    $webform_submission_data = $webform_submission->getData();
    if ($webform->hasPage() === TRUE && $webform->hasWizardPages() === TRUE) {
      $webform_elements = $webform->getElementsDecoded();
      $webform_element_array = [];
      foreach ($webform_elements as $sec_value) {
        if ($sec_value['#type'] == 'webform_wizard_page') {
          foreach ($sec_value as $field_value) {
            if (is_array($field_value)) {
              foreach ($field_value as $f_name => $f_value) {
                $sec = str_replace('/', ' ', $sec_value['#title']);
                $field_v = str_replace('/', ' ', $field_value['#title']);
                if (is_array($f_value) && $f_value['#type'] == 'webform_document_file') {
                  $f_v = str_replace('/', ' ', $f_value['#title']);
                  $webform_element_array[$sec][$field_v][$f_v] = $webform_submission_data[$f_name];
                }
                elseif (is_array($f_value) && $f_value['#type'] == 'webform_section') {
                  foreach ($f_value as $k => $v) {
                    if (is_array($v) && $v['#type'] == 'webform_document_file') {
                      $f_v = str_replace('/', ' ', $f_value['#title']);
                      $v_v = str_replace('/', ' ', $v['#title']);
                      // @todo The below expression contains identical operands, what is this supposed to do?
                      $webform_element_array[$sec][$field_v][$f_v][$v_v] = is_array($webform_submission_data[$k]) ? $webform_submission_data[$k] : $webform_submission_data[$k];
                    }
                  }
                }
              }
            }
          }
        }
      }

      // Add webform_zip Folder.
      $zip_folder_path = $private_file = $this->fileSystem->realpath('private://') . '/webform_zips';
      $this->fileSystem->prepareDirectory($private_file, FileSystemInterface::CREATE_DIRECTORY);

      // Add Webform ID Folder.
      $private_file = $this->fileSystem->realpath('private://') . '/webform_zips/' . $webform_id;
      $this->fileSystem->prepareDirectory($private_file, FileSystemInterface::CREATE_DIRECTORY);

      // Add Submission ID Folder.
      $inner_folder = array_key_exists('project_name', $webform_submission_data) && !empty($webform_submission_data['project_name']) ? $webform_submission_id . '-' . $webform_submission_data['project_name'] : $webform_id . '_' . $webform_submission_id . '_files';
      $private_file = $this->fileSystem->realpath('private://') . '/webform_zips/' . $webform_id . '/' . $inner_folder;
      $this->fileSystem->prepareDirectory($private_file, FileSystemInterface::CREATE_DIRECTORY);

      foreach ($webform_element_array as $page_element) {
        foreach ($page_element as $sec => $sec_value) {
          $sec_path = $private_file . '/' . $sec;
          $this->fileSystem->prepareDirectory($sec_path, FileSystemInterface::CREATE_DIRECTORY);
          if (is_array($sec_value)) {
            foreach ($sec_value as $k => $v) {
              if (is_array($v)) {
                $k_path = $sec_path . '/' . $k;
                $this->fileSystem->prepareDirectory($k_path, FileSystemInterface::CREATE_DIRECTORY);
              }
            }
          }
        }
      }

    }
    else {
      $get_attachments = $webform->getElementsAttachments();
      $submissions = $webform_submission_data;

      $file = [];
      foreach ($get_attachments as $file_field) {
        $fids = $submissions[$file_field];
        if (!is_array($fids) && !empty($fids) && is_numeric($fids)) {
          $fids = [$fids];
        }
        if (!empty($fids)) {
          $file = array_merge($file, $fids);
        }
      }

      $private_file = $this->fileSystem->realpath('private://') . '/webform_zips/' . $webform_id;
      $this->fileSystem->prepareDirectory($private_file, FileSystemInterface::CREATE_DIRECTORY);

      $file_path = $private_file . '/' . $filename . '.zip';

      if (!empty($file)) {
        $file_zip = new Zip($file_path);
        foreach ($file as $f) {
          $file_obj = $this->entityTypeManager->getStorage('file')->load($f);
          if ($file_obj) {
            $file = $this->fileSystem->realpath($file_obj->getFileUri());
            $file_zip->add($file);
          }
        }

        if ($file_zip instanceof Zip) {
          if (empty($file_zip->listContents())) {
            $file_zip->close();
            return new RedirectResponse($webform_submission->toUrl()->toString());
          }
          $file_zip->close();

          $response = new Response();
          $response->headers->set('Content-Type', 'application/octet-stream');
          $response->headers->set('Content-disposition', 'attachment; filename=' . urlencode($filename . '.zip'));
          $response->headers->set('Content-Transfer-Encoding', 'binary');
          $response->setContent(file_get_contents($file_path));
          unlink($file_path);
          return $response;
        }
      }
      else {
        $this->messenger->addStatus(t('No File available for download'));
        $redirect_url = '/admin/structure/webform/manage/' . $webform_id . '/results/submissions';
        $response = new RedirectResponse($redirect_url);
        $response->send();
        return $response;
      }
    }

    chdir($zip_folder_path . '/' . $webform_id);
    $zip = new \ZipArchive();
    $filename = array_key_exists('project_name', $webform_submission_data) && !empty($webform_submission_data['project_name']) ? $webform_submission_data['project_name'] : $webform_id . '_' . $webform_submission_id . '_files';
    $filename = $filename . '.zip';
    if ((file_exists($filename) && $zip->open($filename, \ZipArchive::OVERWRITE) !== TRUE) || $zip->open($filename, \ZipArchive::CREATE) !== TRUE) {
      exit("cannot open <$filename>\n");
    }
    $inner_folder = array_key_exists('project_name', $webform_submission_data) && !empty($webform_submission_data['project_name']) ? $webform_submission_id . '-' . $webform_submission_data['project_name'] : $webform_id . '_' . $webform_submission_id . '_files';
    $this->createZip($zip, $inner_folder . '/');
    $zip->close();
    if (file_exists($filename)) {
      $response = new Response();
      $response->headers->set('Content-Type', 'application/zip');
      $response->headers->set('Content-disposition', 'attachment; filename=' . basename($filename));
      $response->headers->set('Content-Transfer-Encoding', 'binary');
      $response->setContent(file_get_contents($filename));

      unlink($filename);
      $this->removeDir($inner_folder);
      return $response;
    }
    else {
      $this->messenger->addStatus(t('No File available for download'));
      $redirect_url = '/admin/structure/webform/manage/' . $webform_id . '/results/submissions';
      $response = new RedirectResponse($redirect_url);
      $response->send();
      // $response = new Response();
      return $response;
    }

  }

  /**
   * {@inheritdoc}
   */
  function createZip($zip,$dir) {
    if (is_dir($dir)) {
      if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false){
          // If file
          if (is_file($dir.$file)) {
            if($file != '' && $file != '.' && $file != '..'){
              $zip->addFile($dir.$file);
            }
          }
          else {
            // If directory
            if (is_dir($dir.$file) ) {
              if ($file != '' && $file != '.' && $file != '..') {
                // Add empty directory
                $zip->addEmptyDir($dir . $file);
                $folder = $dir . $file . '/';
                // Read data of the folder
                $this->createZip($zip,$folder);
              }
            }
          }
        }
        closedir($dh);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  function removeDir($target) {
    $directory = new RecursiveDirectoryIterator($target,  \FilesystemIterator::SKIP_DOTS);
    $files = new \RecursiveIteratorIterator($directory, \RecursiveIteratorIterator::CHILD_FIRST);
    foreach ($files as $file) {
      if (is_dir($file)) {
        rmdir($file);
      } else {
        unlink($file);
      }
    }
    rmdir($target);
  }

}
